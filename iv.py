"""Script to measure I(V) Characteristics"""
#pylint: disable=invalid-name

import time
import yaml
import numpy as np
from Devices.Keithley6517A import Keithley6517a
from Plotter import Plotter

def main():
    """main"""
    print("\t\t\t============================")
    print("\t\t\tMeasure I(V) characteristics")
    print("\t\t\t============================")

    with open('labExcersiseConfig.yaml') as cfg_file:
        config = yaml.load(cfg_file, Loader=yaml.FullLoader)

    #start stop step
    start = config["Measurements"]["IV"]["min"]
    stop = config["Measurements"]["IV"]["max"]
    step = config["Measurements"]["IV"]["step"]

    #Data list to be plotted
    voltage_list = []
    current_list = []
    ivPlotter = Plotter()
    ivPlotter.initIvPlot()

    #Initialize Picoamperemeter
    HVSupply = Keithley6517a(config["Keithley6517A"])
    HVSupply.initDevice()

    #I(V) curve Loop
    with open('Data/IV_data.txt', 'w') as dat_file:
        dat_file.write("Voltage (V)\tCurrent(A)\n")

        for voltage in [round(i, 1) for i in np.arange(start, stop, step)]:
            HVSupply.set_voltage(voltage)
            time.sleep(0.2)

            current = HVSupply.read_current()
            data = str(voltage) + "\t" + str(current)
            print(data)
            dat_file.write(data + "\n")

            voltage_list.append(voltage)
            current_list.append(current)
            ivPlotter.updateIvPlot(voltage_list, current_list)

        HVSupply.stop()
        ivPlotter.displayPlot()

if __name__ == '__main__':
    main()
