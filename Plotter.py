import numpy as np
import matplotlib.pyplot as plt

class Plotter():
    def __init__( self ):
        pass

    def initMonitor ( self ):
        self.fig, self.axs = plt.subplots(3, 2, figsize=(6,8))
        self.fig.canvas.manager.window.wm_geometry("+1300+100")
        self.fig.canvas.set_window_title("Monitor")
        self.axs[0, 0].set_title("Sensor Temperature (°C)")
        self.axs[1, 0].set_title("Peltier Temperature (°C)")
        self.axs[0, 1].set_title("Air Temperature (°C)")
        self.axs[1, 1].set_title("Dew Point (°C)")
        self.axs[2, 1].set_title("Humidity (%)")
        self.axs[2, 0].set_title("Summary")

        self.fig.tight_layout()


    def updateMonitor( self, pTime, pSensorTemperature, pPeltierTemperature, pAirTemperature, pDewPoint, pHumidity ):
        self.axs[0, 0].cla()
        self.axs[0, 0].set_title("Sensor Temperature (°C)")
        self.axs[0, 0].plot(pTime, pSensorTemperature, lw=2, color='red')
        self.axs[1, 0].cla()
        self.axs[1, 0].set_title("Peltier Temperature (°C)")
        self.axs[1, 0].plot(pTime, pPeltierTemperature, lw=2, color='orange')
        self.axs[0, 1].cla()
        self.axs[0, 1].set_title("Air Temperature (°C)")
        self.axs[0, 1].plot(pTime, pAirTemperature, lw=2, color='green')
        self.axs[1, 1].cla()
        self.axs[1, 1].set_title("Dew Point (°C)")
        self.axs[1, 1].plot(pTime, pDewPoint, lw=2, color='blue')
        self.axs[2, 1].cla()
        self.axs[2, 1].set_title("Humidity (%)")
        self.axs[2, 1].plot(pTime, pHumidity, lw=2, color='yellow')
        self.axs[2, 0].cla()
        self.axs[2, 0].set_title("Summary")
        summary = "Time:\n" + "Sensor:\n"+ "Peltier:\n" + "Air:\n" + "DewPoint:\n" + "Humidity:"
        values = str(pTime[-1]) + " s\n" + "{:.2f}".format(pSensorTemperature[-1]) + " °C\n" + "{:.2f}".format(pPeltierTemperature[-1]) + " °C\n" + "{:.2f}".format(pAirTemperature[-1]) + " °C\n" +"{:.2f}".format(pDewPoint[-1]) + " °C\n" + "{:.2f}".format(pHumidity[-1]) + " %"
        self.axs[2, 0].text(0.1,0.3,summary)
        self.axs[2, 0].text(0.4,0.3,values)
        plt.pause(0.01)
        self.fig.canvas.draw()

    def initIvPlot( self ):
        self.ivFig = plt.gcf()
        self.ivFig.show()
        self.ivFig.canvas.draw()
        self.ivFig.canvas.set_window_title("I(V) Measurement")

        plt.xlabel('Voltage (V)')
        plt.ylabel('Current (A)')
        plt.title("I(V) Measurement")
        plt.grid(True)

    def updateIvPlot( self, pDataX, pDataY ):
        plt.plot(pDataX, pDataY, lw=2, color='red')
        plt.pause(0.01)
        self.ivFig.canvas.draw()

    def initCvPlot( self ):
        self.cvFig = plt.gcf()
        self.cvFig.show()
        self.cvFig.canvas.draw()
        self.cvFig.canvas.set_window_title("C(V) Measurement")

        plt.xlabel('Voltage (V)')
        plt.ylabel('1/C² (1/F²)')
        plt.title("C(V) Measurement")
        plt.grid(True)

    def updateCvPlot( self, pDataX, pDataY ):
        y_quad = []
        for y in pDataY:
            y_quad.append(1/(y*y))
        plt.plot(pDataX, y_quad, lw=2, color='red')
        plt.pause(0.01)
        self.cvFig.canvas.draw()

    def initRpolyPlot( self ):
        self.rpolyFig = plt.gcf()
        #self.rpolyFig.show()
        self.rpolyFig.canvas.draw()
        self.rpolyFig.canvas.set_window_title("R_poly Measurement")

        plt.xlabel('Low Voltage (V)')
        plt.ylabel('Polyresistor Current')
        plt.title("R_poly Measurement")
        plt.grid(True)

    def updateRpolyPlot(self, pDataX, pDataY):
        plt.plot(pDataX, pDataY, lw=2, color='red')
        plt.pause(0.01)
        if len(pDataX) > 2:
            m, b = np.polyfit(pDataX,pDataY, 1)
            x = np.linspace(pDataX[0],pDataX[-1],10)
            y = [x_v * m + b for x_v in x ]
            plt.plot(x,y, ':' , color = 'blue')
            rpoly = str(  "{:.4f}".format((1.0/m) / 1000000.0) ) + " M\u03A9"
            for txt in self.rpolyFig.texts:
                txt.set_visible(False)
            plt.figtext(0.15, 0.85,rpoly, horizontalalignment='left', verticalalignment='top',wrap=True ,  color= "blue")
            plt.pause(0.01)
            print("Rpoly:\t" + rpoly )

        self.rpolyFig.canvas.draw()


    def initRintPlot( self ):
        self.rintFig = plt.gcf()
        self.rintFig.show()
        self.rintFig.canvas.draw()
        self.rintFig.canvas.set_window_title("R_int Measurement")

        plt.xlabel('Low Voltage (V)')
        plt.ylabel('DC - DC Current')
        plt.title("Rint Measurement")
        plt.grid(True)
        plt.legend()


    def updateRintPlot( self, pDataX, pDataY ):
        plt.plot(pDataX, pDataY, lw=2, color='red')
        plt.pause(0.01)
        if len(pDataX) > 2:
            m, b = np.polyfit(pDataX,pDataY, 1)
            x = np.linspace(pDataX[0],pDataX[-1],10)
            y = [x_v * m + b for x_v in x ]
            plt.plot(x,y, ':' , color = 'blue')
            rint = str(  "{:.4f}".format((1.0/m) / 1e9) ) + " G\u03A9"
            for txt in self.rintFig.texts:
                txt.set_visible(False)
            plt.figtext(0.15, 0.85,rint, horizontalalignment='left', verticalalignment='top',wrap=True ,  color= "blue")
            plt.pause(0.01)
            print("Rint:\t" + rint )
        self.rintFig.canvas.draw()


    def initCcPlot( self ):
        self.ccFig = plt.gcf()
        self.ccFig.show()
        self.ccFig.canvas.draw()
        self.ccFig.canvas.set_window_title("Coupling Capacitance Measurement")

        plt.xlabel('Bias Voltage (V)')
        plt.ylabel('Coupling Capacitance (F)')
        plt.title("CC Measurement")
        plt.grid(True)

    def updateCcPlot( self, pDataX, pDataY ):
        plt.plot(pDataX, pDataY, lw=2, color='red')
        plt.pause(0.01)
        self.ccFig.canvas.draw()

    def displayPlot( self ):
        plt.show()
