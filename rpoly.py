"""Scipt to measure R_poly value"""
#pylint: disable=invalid-name

import time
import yaml
import numpy as np
from Devices.Keithley6517A import Keithley6517a
from Devices.Zup import Zup
from Plotter import Plotter



def main():
    """main"""
    print("\t\t\t==============================")
    print("\t\t\tMeasure R_poly characteristics")
    print("\t\t\t==============================")

    with open('labExcersiseConfig.yaml') as configFile:
        config = yaml.load(configFile, Loader=yaml.FullLoader)

    lvSupply = Zup(config["ZupLV"])
    hvSupply = Keithley6517a(config["Keithley6517A"])
    hvSupply.initDevice(pCompliance="1E-5")

    lowVoltage = []
    polyCurrent = []
    rpolyPlotter = Plotter()
    rpolyPlotter.initRpolyPlot()

    #start stop step
    start = config["Measurements"]["R_poly"]["min"]
    stop = config["Measurements"]["R_poly"]["max"]
    step = config["Measurements"]["R_poly"]["step"]

    #Deplete sensor
    hvSupply.ramp_voltage(config["Measurements"]["R_poly"]["biasvoltage"])

    #LV ramp at polyresistor
    with open('Data/R_poly_data.txt', 'w') as dat_file:
        dat_file.write("Voltage (V)\tCurrent(A)\n")

        for voltage in [round(i, 1) for i in np.arange(start, stop, step)]:
            lvSupply.set_voltage(voltage)
            time.sleep(0.2)
            current = hvSupply.read_current()
            data = str(voltage) + '\t' + str(current)
            print(data)
            dat_file.write(data + '\n')
            lowVoltage.append(voltage)
            polyCurrent.append(current)
            rpolyPlotter.updateRpolyPlot(lowVoltage, polyCurrent)

    lvSupply.stop()
    hvSupply.stop()
    rpolyPlotter.displayPlot()


if __name__ == '__main__':
    main()
