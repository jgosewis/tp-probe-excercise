"""The GPIO module

The control of the Raspberry Pi GPIO pins is
implemented in this module.
"""
import RPi.GPIO as GPIO
import time

class PiGPIO():
      
    def __init__(self):
        """Initialize the GPIO pins"""
        self.pins = [19]
        GPIO.setmode(GPIO.BCM)
        [GPIO.setup(pin,GPIO.OUT) for pin in self.pins]
              
    def set_positive_polarity(self):
        """set the selected pin "high" (3.3V)"""
        pin = self.pins[0]
        if pin in self.pins:
            GPIO.output(pin,1)
        else:
            print("pin {0} not initialized".format(pin))    
        
    def set_negative_polarity(self):
        """set the selected pin "low" (0V)"""
        pin = self.pins[0]
        if pin in self.pins:
            GPIO.output(pin,0)
        else:
            print("pin {0} not initialized".format(pin))

test = PiGPIO()

test.set_positive_polarity()
time.sleep(2)
test.set_negative_polarity()