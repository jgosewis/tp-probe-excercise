from Devices.Utils import SerialPort


class Keithley6517a():
    def __init__(self, pSettings):
        pSettings["ReadTermination"] = "\r\n"
        self.serialPort = SerialPort( pSettings )


    def initDevice(self, pCompliance = "1E-7"):
        self.serialPort.write("*CLS")
        # reset keithley
        self.serialPort.write("*RST")
        # zero check off
        self.serialPort.write("SYST:ZCH OFF")
        # set voltage range
        self.serialPort.write("SOUR:VOLT:RANG 1000")
        self.serialPort.write("CURR:RANG " + pCompliance)
        # set initial voltage
        #self.serialPort.write("SOUR:VOLT 10")
        self.serialPort.write("SENS:FUNC 'CURR'")
        self.serialPort.write("TRIG:DEL 0")
        # trigger timer
        self.serialPort.write("TRIG:TIM 0.003")
        # number of points retrived
        self.serialPort.write("TRAC:POINts 1")
        # set output voltage on (attention, now the voltage is on)
        self.serialPort.write("OUTP ON")

    def stop( self ):
        self.ramp_down()

    def read_current(self):
        """Read the current"""
        self.serialPort.write("TRAC:CLE")
        self.serialPort.write("INIT:CONT ON")
        # get the current. trac controls configure and control
        # data storage into the buffer.
        self.serialPort.write("TRAC:FEED:CONT NEXT")


        # retrieve readings stored in the buffer
        line = self.serialPort.read("TRAC:DATA?")
        splitted = line.split("N")
        try:
            return float(splitted[0])
        except:
            return 0

    def set_voltage(self, voltage=0):
        # set source to voltage
        self.serialPort.write("SOUR:VOLT %F" %(float(voltage)))


    def ramp_voltage(self, voltage=0, stepsize=10):

        bias = float(self.serialPort.read("SOUR:VOLT?"))

        # check if stepsize should be     negative
        if voltage < bias:
            stepsize = -abs(stepsize)

        if voltage < bias:
            while voltage < bias:
                self.set_voltage(bias)
                #current = float(self.read_current())
                bias += stepsize
        else:
            while bias < voltage:
                self.set_voltage(bias)
                #current = float(self.read_current())
                bias += stepsize

        self.set_voltage(voltage)

    def read_voltage(self):
        return float(self.serialPort.read("SOUR:VOLT?"))

    def ramp_down(self, stepsize=10):

        # read voltage
        bias = float(self.serialPort.read("SOUR:VOLT?"))
        # check that if negative voltage, stepsize should be smaller than zero
        if bias < 0:
            stepsize = -abs(stepsize)
        # ramp down with steps of 10 volts
        voltage = bias - (bias % stepsize)
        self.set_voltage(voltage)
        while abs(voltage) > 0.00001:
            voltage -= stepsize
            self.set_voltage(voltage)

        self.serialPort.write("SOUR:VOLT 0")
        # Close settings
        self.serialPort.write("OUTP:STAT 0")
        # output voltage off
        self.serialPort.write("OUTP OFF")
        # set zero check on
        self.serialPort.write("SYST:ZCH ON")
        # reset
        self.serialPort.write("*RST")
        # clear
        self.serialPort.write("*CLS")
        # system preset
        self.serialPort.write("SYST:PRES")
        # system exit
        self.serialPort.write("SYST:KEY 11")
