"""Script to measure the interstrip capacitance"""
#pylint: disable=invalid-name

import time
import yaml
from Devices.Keithley6517A import Keithley6517a
from Devices.HM8118 import HM8118

def main():
    """main"""
    print("\t\t\t============================")
    print("\t\t\tMeasure C_int characteristics")
    print("\t\t\t============================")

    #load config file
    with open('labExcersiseConfig.yaml') as configFile:
        config = yaml.load(configFile, Loader=yaml.FullLoader)

    #initialize HVSupply
    print("Initialize Keithley6517A")
    HVSupply = Keithley6517a(config["Keithley6517A"])
    HVSupply.initDevice()

    #initialize LCR meter
    print("Initialize HM8118")
    LCR = HM8118(config["HM8118"])
    LCR.setFrequency(config["Measurements"]["C_int"]["frequency"])

    #ramp HV
    HVSupply.ramp_voltage(config["Measurements"]["C_int"]["biasvoltage"])
    #time.sleep(20)

    #open data file and read capacitance
    with open('Data/C_int_data.txt', 'w') as dat_file:
        dat_file.write("Interstrip capacitance (F)\n")
        capacitance = LCR.readCapacitance()
        print(f"Interstrip capacitance: {capacitance}")
        dat_file.write(str(capacitance))

    #rampdown
    HVSupply.ramp_down()

if __name__ == '__main__':
    main()
